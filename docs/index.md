---
hide:
  - navigation
  - toc
---

# Lab de prise en main d'Openstack avec Horizon

* Version en ligne: [openstack-labs-101-horizon.html](openstack-labs-101-horizon.html)
* version PDF: [openstack-labs-101-horizon.pdf](openstack-labs-101-horizon.pdf)

---

![image alt <>](assets/openstack.png)
