---
marp: true
paginate: true
theme: default
---
<!-- #3eaff3 bleu ciel -->
<!-- #0074d0 bleu electrique -->
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:40% 90%](assets/openstack.png)
# Lab : Premiers pas avec Openstack
# Utilisation d'Horizon

-emmanuel.braux@imt-atlantique.fr-

---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
# Objectifs : 

**1. Initialisation de l'environnement**
**2. Création de l'environnement réseau**
**3. Lancement d'une instance**
**4. Création d'une Keypair**
**5. Lancement d'une instance directement fonctionelle**
**6. Création d'un volume**
**7. Suppression des ressources créés**

---
# Licence informations


Auteur : -emmanuel.braux@imt-atlantique.fr-

Cette présentation est sous License Créative Commons 3.0 France (CC BY-NC-SA 3.0 FR)
Selon les options : Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions.

![cc-by-nc-sa](img/cc-by-nc-sa.png)

---

<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 80% ](assets/openstack.png)

## > 1. Initialisation de l'environnement
*2. Création de l'environnement réseau*
*3. Lancement d'une instance*
*4. Création d'une Keypair*
*5. Lancement d'une instance directement fonctionelle*
*6. Création d'un volume*
*7. Suppression des ressources créés*


--- 
![bg left:30% 80% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_accueil.png)
## Se connecter à Horizon

- Url de connexion : [http://localhost:8261/dashboard/](http://localhost:8261/dashboard/)
- login : demo
- mot de passe : stack

---
## Aperçu global
![bg left:60% 90% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_overview.png)
- Services disponibles
- Quotas disponibles
- Ressource sdéployées
- ...


---
![bg right:60% 70% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_settings.png)

## Configurer Horizon


![h:400 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_menu_settings.png)


---
## Choisir son projet !!!

![h:250 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_check_project.png )

---
## Observer les ressources disponibles et quotas

![h:500 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_quotas.png)

---
![bg left:30% 80% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_menu_compute.png)

## Prendre en main l'interface

- Menu « Projet / Compute» 
- Ressources liées au projet
  - Tableau de bord global
  - Vues détaillées : Instances, volumes, images ...
  - Gestion des clé SSH : connexion aux instances
  - Accès aux API : outils client
- C’est le menu le plus utilisé.

*[https://docs.openstack.org/horizon/latest/user/index.html](https://docs.openstack.org/horizon/latest/user/index.html)*

---

## API Access

![bg left:55% 95% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_api.png)
***Afficher les services disponibles***
- identity  = Keystone
- network = Neutron
- compute = Nova
- image = Glance
- ...

---
## Afficher les images disponibles

![h:350 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_check_images.png)

---
## Afficher les gabarits disponibles

- La liste des gabarits n'est pas disponible dans l'interface horizon
- Elle sera acessible au moment de la création d'une instance

![h:400 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_check_flavors.png)

---
## Afficher les Reseaux et subnets disponibles

- "external" : 
  - réseau pour les adresse IP fLottantes
  - impossible d 'y connecter une instance

![h:300 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_check_networks.png)

---

![bg left:45% 95% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_secgroup_inventory.png)

## Afficher les groupes de sécurité disponibles

- IP 4 et IPV6
- Ouvert en sortie
- Fermé en entrée

![drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_secgroup_inventory_detail.png)


---
## Afficher les Ip flottantes affectées au projet

- Par défaut aucune IP flottante n'est associée au projet
  
![h:300 drop-shadow:0,5px,10px,rgba(0,0,0,.4) ](img2/horizon_check_ips.png)


---
## Vérifier les paires de clé SSH

- Par défaut aucune clé n'est créée
    
![h:300 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_check_keypairs.png)



---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 80%](assets/openstack.png)

*1. Initialisation de l'environnement*
## > 2. Création de l'environnement réseau 
*3. Lancement d'une instance*
*4. Création d'une Keypair*
*5. Lancement d'une instance directement fonctionelle*
*6. Création d'un volume*
*7. Suppression des ressources créés*

---
## Objectif :

![bg left:30% 70% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_network_objectif.png)
- Adresse réseau 172.16.1.0/24 (réseau privé)
- Connecté au réseau « External »
- Passerelle 172.16.1.1
- DNS : 192.71.245.208 (DNS Open NIC)


---

## Configuration de départ

![bg left:35% 85% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_network_inventory.png)

- 1 réseau : « External »
- Droits limités
- IP limitées
  
![drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_network_inventory_detail.png)

---

![bg right:50% 90% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_network_create_detail.png)

## Creation du réseau


- **Nom : net-demo**
- **Ne pas créer de sous- réseau**

![drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_network_create.png)


---
## Vérification du réseau
![bg left:40% 80% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_network_result.png)

- Le réseau est créé
- Pas d’adresses IP :
    - **Créer un sous-réseau**
  
---

![bg right:50% 90% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_subnet_create_detail1.png)

## Création d'un sous réseau

- Nom : **subnet-demo'**
- Subnet-range : **172.16.1.0/24**
- Gateway : **172.16.1.1**

![drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_subnet_create.png)  
  
---

![bg left:50% 80% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_subnet_create_detail2.png)

## Détails du sous-réseau 
- DHCP : **Activer**
- DNS : 192.71.245.208

  
---
![bg left:35% 90% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_subnet_result.png)
## Vérification du subnet

- Un réseau
- Une plage d’adresse IP
  
![drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_subnet_result_detail.png)


### **Lien avec l’extérieur ?**
  - **Création d’un routeur**


---
![bg right:50% 95% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_router_create_detail.png)

## Creation du routeur

- **Nom : rt-demo**
- **Réseau externe :  external**


![drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_router_create.png)

---
![bg left:35% 90% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_router_result.png)
## Vérification du routeur


![drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_router_result_detail.png)

### **Pas de lien entre le routeur et le sous-réseau**
  - **Ajouter une interface !**

---
![bg right:50% 95% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_router_interface_create_detail.png)

## Creation d'une interface

- **Sous réseau subnet-demo**
- **Adresse de passerelle : vide**
*(celle par défaut du sous réseau)*


![h:150 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_router_interface_create1.png)
![h:150 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_router_interface_create2.png)

---
![bg left:35% 90% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_router_interface_result.png)
## Vérification de l'interface

- **Tout est en place !!**
  
![drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_router_interface_result_detail.png)


---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 80%](assets/openstack.png)

*1. Initialisation de l'environnement*
*2. Création de l'environnement réseau*
## > 3. Lancement d'une instance
*4. Création d'une Keypair*
*5. Lancement d'une instance directement fonctionelle*
*6. Création d'un volume*
*7. Suppression des ressources créés*


---
## Lancement d'une instance

- Instance de test (Ping et SSH)
- Mini système Linux : "cirros"
  
![h:250 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_server_create.png)  

---
## Déploiement

![bg left:55% 90%  drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_server_details.png)

- **Nom de l'instance : demo1**

> Observer les information obligatoire signalées par "*"
---
## Selection de la source

- **!! Ne pas créer de volume !!**
- **Image : cirros**

  
![bg left:50% 80% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_server_source_choice.png)

---

### Selection de la source : 

## **Vous devez obtenir :**

![bg right:55% 80% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_server_source_result.png)

---
## Selection du Gabarit

![h:400 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_server_flavor.png)

---
## Selection du réseau auquel se connecter

![h:350 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/horizon_server_network.png)

---
### Tous les champs obligatoires sont remplis

## **Création de l'instance**

![bg right:55% 90%  drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_server_launch.png)

---
## Lancement de l'instance

![h:150 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_server_plan.png)

![h:170 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_server_generate.png)

![h:190 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_server_started.png)



---
## Accès à la console

- Pas d'accès activé (Problème de configuartion avec le NAT de VirtualBox)
- Très peu utilisé car : 
  - pas ergonomique
  - pas de copier/coller
- Utilisée principalement pour vérifier le bon fonctionnement des instances 
- Pas adapté à des usages de type bureau distant (VDI).


---  
## Connexion SSH

Les instances ne sont accessibles que depuis la VM du LAB.

Il faut donc se connecter à cette VM, pour pouvoir accéder à l'instance déployée.

Exemple de commande :
`ssh -p 2261 stack@localhost`

Une fois connecteé à la VM, il est possible de faire les test de connexion à l'instance, en utilisant la aussi le protocle ssh.

---

## Se connecter à l’instance
![bg right:50% 90% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_server_privateip.png)

- Obtenir l'adresse IP 
  *(ici 172.16.1.9)*
- Se connecter en SSH

```
ssh cirros@172.16.1.9
```

---

# Pourquoi ca ne fonctionne pas ...

- L’instance fonctionne ?
- Le SSH fonctionne ?
- Le « ping » fonctionne ?
- Le réseau est-il correctement configuré ?

---
![bg right:45% 80% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_server_checknetwork.png)

# Vérifier le réseau ...

- Pour que l’instance soit visible sur le réseau externe
 ...
- Il faut lui associer une **IP Flottante**

---
![bg left:40% 90% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_fip_allocation.png)

# Allouer une IP flottante

- **pool : external**


![h:200 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_fip_allocation_details.png)

---
# Allouer une IP flottante

![h:250 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_fip_allocation_result.png)

> **Ici l'IP flottante est 203.0.113.224**

---

# Associer une IP flottante à l'instance


![h:230 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_server_FIP_add.png)
![h:230 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_server_FIP_add_detail.png)

---
# Connexion avec l’IP flottante
![bg left:50% 80% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_server_fip.png)

- Obtenir l'adresse IP flottante
- se connecter en SSH

```
ssh cirros@203.0.113.224
```

---

# Pourquoi ca ne fonctionne pas ...

- L’instance fonctionne ?
- Le réseau est-il correctement configuré ?
- ...

**C'est quoi une connexion SSH ?**

---

![bg right:45% 80% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_secgroup_server_detail.png)

# Afficher les groupes de sécurité de l'instance

![h:200 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_secgroup_server.png)

- IP 4 et IPV6
- Ouvert en sortie /  Fermé en entrée
- Besoin  : 
**Ouvrir l'accès pour le PING et le SSH**

---

![bg right:45% 95% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_sg_create_details.png)
# Création d'un groupe de sécurité

- **Nom :**
  - **sg-admin-access**
- **Description :**
  - **Ouverture des accès admin - PING et SSH**

![drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_sg_create.png)

---

![bg right:40% 90% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_sg_add_rule.png)

# Définition des régles 

- ping : ICMP
- SSH : TCP, port 22
  
  
![drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_sg_manage_rules.png)

---
# Les règles de sécurité

- Règle de filtrage standard
    - Protocole
    - Port
    - Source
    - Destination
    - 
![bg left:55% 90% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_sg_rules_detail.png)

--- 

# Règles de sécurité 
![bg right:45% 90% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_sg_rules_ping.png)
![bg left:45% 90% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_sg_rules_ssh.png)

- SSH :

- PING :


![drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_sg_rules_result.png)

--- 

![bg right:40% 90% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_server_sg_association2.png)

# Affecter un groupe de sécurité à l'instance

![ drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_server_sg_association1.png)

--- 
# Test PING et connexion SSH

```
ssh cirros@10.29.244.14
```

- Normalement, les informations suivantes devraient être utiles :
  -  login : **cirros**
  -  password : **gocubsgo**

> Rem: Le message ci dessous apparait lorsque qu'on se connecte pour la première fois à un serveur Linux. Il permet d' "enregistrer la signature du serveur". Saisir 'yes'
``` bash
The authenticity of host '203.0.113.224 (203.0.113.224)' can't be established.
ECDSA key fingerprint is SHA256:UEhDiXnQlUQMujKjaYNo0wVljlMGO8l4ATsjIBUdAFA.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])?  yes
```
---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 80%](assets/openstack.png)

*1. Initialisation de l'environnement*
*2. Création de l'environnement réseau*
*3. Lancement d'une instance*
## > 4. Création d'une Keypair
*5. Lancement d'une instance directement fonctionelle*
*6. Création d'un volume*
*7. Suppression des ressources créés*


---
# Utilisation d'une Keypair

- Se connecter en SSH sans utiliser de mot de passe
  - sécurité
  - automatisation
- La plupart des images cloud ne permettent pas de se connecter avec un mot de passe, mais uniquement avec une clé SSH
- Une keypair est consitué :
  - d'une clé publique : conservée par Openstack, et "injectée" dans les instances, et accessible via l'interface ou en ligne de commande
  - d'une clé privée : qui vous est transmise, et qui ne peut pas être regénérée en cas de perte.
  
---
# Création d'une Keypair avec Openstack
 
![h:250 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_keypair.png)
- On récupère la clé privée : un fichier "pem"
  - **!! A conserver dans un endroit sécurisé !!**

![drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_keypair_save.png)

> **La clé est liée à l'utilisateur, pas au projet**

---
# Openstack conserve la clé publique

![drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_keypair_public.png)


---
# Transfert de la clé privée sur la VM du LAB

- La clé a été générée depuis le navigateur
- Elle a donc été copiée sur vote poste de TP
- Mais elle doit étre utilisée sur la machine du LAB
- Il faut donc la copier, en utilisant la commande `scp`

Exemple de commande :
`scp -P 2261 ~/Downloads/demo.pem stack@localhost:/home/stack`


---
# Sécurisation d'une keypair

- Dans un système Linux :
  - Les droits du fichier pem doivent être ajustés, pour pouvoir utiliser la clé.
  - Accès uniquement pour le propriétaire de la clé:

  ```
  chmod 600 ~/demo.pem
  ```

---
# Alternative : Utiliser une keypair existante

Vous pouvez importer une clé publique existante, via l'option "Importer une paire de clé": 
- Si vous disposez déjà d'une keypair
- Ou si vous préfèrez générer vous même une keypair


*Rappel, pour générer une keypair sur un système linux.*
```
  ssh-keygen -t rsa -b 4096 -C "<comment>"
```

---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 80%](assets/openstack.png)

*1. Initialisation de l'environnement*
*2. Création de l'environnement réseau*
*3. Lancement d'une instance*
*4. Création d'une Keypair*
## > 5. Lancement d'une instance directement fonctionelle
*6. Création d'un volume*
*7. Suppression des ressources créés*


---

# Instance avec connexion sans mot de passe

- Bonne pratique : **On ne modifie pas une instance !!**
- Supprimer l'instance créée
- Récréer une instance, en précisant lors de la création
  - la "Clé SSH" 
  - le "Groupe de sécurité"


**Seule l'IP flottante doit être affectée une fois l'instance lancée.**

---

# Selection de la paire de clé

- Si une seule clé est présente, Horizon la selectionne par défaut.
- Si plusieurs clés sont présentes, aucune n'est selectionnée
  
![h:450 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_server_key.png)

---

# Test PING et connexion SSH

```
ssh -i ~/demo.pem cirros@203.0.113.224
```

- Normalement, plus besoin de mot de passe.
-  Mais un message d'erreur.


``` bash
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!                                    @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
Someone could be eavesdropping on you right now (man-in-the-middle attack)!
It is also possible that a host key has just been changed.
...
Add correct host key in ~/.ssh/known_hosts to get rid of this message.
Offending ECDSA key in ~/.ssh/known_hosts:1
Host key for 203.0.113.224 has changed and you have requested strict checking.
Host key verification failed.
```
---
# Protection  "man-in-the-middle attack"

- Une nouvelle instance
- La signature de l'instance derrière l'IP 203.0.113.224 a donc changé
- La vérification échoue
- Il faut supprimer l'enregistrement correspondant
- `~/.ssh/known_hosts:1` indique qu'il faut supprimer la ligne 1

``` bash
sed -i '1d' ~/.ssh/known_hosts
```
---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 80%](assets/openstack.png)

*1. Initialisation de l'environnement*
*2. Création de l'environnement réseau*
*3. Lancement d'une instance*
*4. Création d'une Keypair*
*5. Lancement d'une instance directement fonctionelle*
## >  Création d'un volume
*7. Suppression des ressources créés*


---
## Creation du volume


![h:250 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_volume_create.png)

---
# Configuration du Volume

![bg left:50% 85% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_volume_create-details.png)

- **Nom : demo_data1**
- **Taille :  1Go**
- **Type : "LVM"**
    
---
# Vérification de la création


![h:400 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_volume_create-resultat.png)

---
# Attachement à une instance

![bg right:45% 90% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_volume_attache-volume.png)

![drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img2/horizon_volume_manage.png)

- Selectionner une instance dans la liste


---
# Vérifier l'attachement à l'instance

**Au niveau du volume** :

![bg:400 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/horizon_volume_attache-volume-resultat.png)

---
# Vérifier l'attachement à l'instance

**Au niveau de l'instance** :

![bg left:55% 85% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/horizon_volume_attach-instance-resultat.png)

---
## Alternative : Attachement depuis l'instance


![h:280 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/horizon_volume_attach-instance.png)

![h:260 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/horizon_volume_attach-instance-detail.png)

 
---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 80%](assets/openstack.png)

*1. Initialisation de l'environnement*
*2. Création de l'environnement réseau*
*3. Lancement d'une instance*
*4. Création d'une Keypair*
*5. Lancement d'une instance directement fonctionelle*
*6. Création d'un volume*
## > Suppression des ressources créés


---

## Suppression de l'instance
- supprimer le volume
- supprimer l'instance

## Suppression de l'infra Réseau

- libérer l'IP flottante
- routeur : subnet Interface
- routeur : passerelle
- routeur
- subnet
- network
- groupe de sécurité

  
 
